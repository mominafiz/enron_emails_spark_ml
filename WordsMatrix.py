﻿import os
import random
import sys
from pyspark import SparkContext, SparkConf
from DataPreProcess import pathExists
import unicodedata
import re
from operator import add


def createMatrix(textRDD, username):
    
    # remove unicode
    textRDD = textRDD.map(lambda x: unicodedata.normalize('NFKD',x).encode('ascii','ignore'))
    
    # split each instance and remove None or empty string
    def removeNone(inst):
        wlist = []
        for i in inst:
            if i is not None or i != '':
                wlist.append(i.strip())
        return wlist
                
    textRDD = textRDD.map(lambda x: x.split(" ")).map(removeNone) 

    # persist data in memory for generating various stats
    textInfos = textRDD.persist()

    unique_words = textInfos.flatMap(lambda x:x).distinct().collect()
    
    # Map Each words
    def mapEachWords(inst):
        wvlist = []
        count = []
        for v in inst:
            if len(wvlist) == 0 and len(count) == 0:
                wvlist.append(v)
                count.append(1)
            else:
                try:
                    i = wvlist.index(v)
                    count[i] += 1
                except:
                    wvlist.append(v)
                    count.append(1)

        return zip(wvlist,count)

    textInfos = textInfos.map(mapEachWords).collect()
    #textInfos = textInfos.collect()

    with open("WordMatrix/WordMatrix_" + username + ".txt","w") as outfile, open("WordMatrix/MatrixHeaders.txt","w") as headout:
        headout.write(' '.join(unique_words))
        for textinfos in textInfos:
            row_template = ['0' for i in xrange(len(unique_words))]
            for word_tuple in textinfos:
                index = unique_words.index(word_tuple[0])
                row_template[index] = str(word_tuple[1])

            outfile.write(' '.join(row_template) + '\n')
        
def checkArgs(args):

    if args[-1].isdigit() and len(args) == 3 and pathExists(args[-2]):
        return args[-1], args[-2]
    else:
        print "Usage: spark-submit WordsMatrix.py <directory path> <# of users> "
        print "  e.g spark-submit WordsMatrix.py /path/to/dir 10"
        print " To select all users, please enter 0 for # of user"
        

def pathExists(path):
    if os.path.exists(path):
        return True
    else: 
        print "Root directory does not exists."
        sys.exit(1)
          

def getUsers(n=0):
    users = []
    with open("UserAndEmail.txt","r") as infile:
        while True:
            try:
              line = infile.next()
              lsplit = line.split(",")
              users.append(tuple([lsplit[0].strip(),lsplit[1].strip()]))
            except StopIteration:
                break
    if n == 0:
        return users

    return random.sample(users,n)
    

def main(sc): 
    rtuple = checkArgs(sys.argv)
    if rtuple is None :
        sys.exit(1)
    
    n, data_path = rtuple
   
    if not pathExists(data_path):
        print "Unable to find directory."
        sys.exit(1)
    
    
    user_dirs = getUsers(int(n))
        
    for user_dir in user_dirs:
        print user_dir
        createMatrix(sc.textFile(os.path.join(data_path,user_dir[0] + "_text.txt")), user_dir[0])

    sc.stop()

if __name__ == "__main__":
    conf = SparkConf().setAppName("WordMatrix")
    
    #in cluster this will be like
    #"spark://ec2-0-17-03-078.compute-#1.amazonaws.com:7077"
    conf = conf.setMaster("local[*]")
    
    #conf = {'fs.s3n.awsAccessKeyId': '', 'fs.s3n.awsSecretAccessKey':''}

    sc = SparkContext(conf=conf)
    main(sc)