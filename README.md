Hands-on experience with the use of AWS and of Machine Learning tools on AWS (Spark) on Enron Email Dataset (https://www.cs.cmu.edu/~./enron/). 
All the necessary algorithms and operations are available in the Spark and MLlib. 
Documentation about Spark's LDA and K-means implementations here http://spark.apache.org/docs/latest/mllib-clustering.html.

# Objectives #:
**Data Preprocessing:**
* Remove header information in each file, also don't forget the headers of previous messages in context!:* o Remove attachment information in each file if exists.
* Remove stopping words as well.
* Process each email as a document.
* Use all files each user.
* Upload your data to Hadoop or Amazon S3 to use it with your Spark cluster.

**Data Discovery**
* Randomly select 10 users (Next steps will be completed with data of randomly selected usero s).
* Provide summary/statistics about each user’s data, e.g. Name, Email Address, Number of Emails, Average Length of Each Email,o  etc.
* (Optional) Try to discover how many users do they communicated with.

**Data Analysis (You should use Apache Spark for this step)
*** For each user; Run K-means and LDA algorithms over user's emails.
* Justify and discuss your input parameters for both algorithms. What values you used and how did you decide?
* After each algorithms execution, get the list of the emails for each group/cluster to compare results.
* Compare the results of LDA and K-means. Are the document distribution across clusters similar? What are the execution times? Did you try different input parameters if so how does it affect the execution times?
* After LDA, print most popular topics and their terms. Does those terms provide you any information regarding user, e.g. user's activities, roles, department, etc.?