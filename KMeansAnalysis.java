package kmeans.kmeans;

import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.spark.SparkConf;

public class KMeansAnalysis {
	public static void main(String[] args) {
		SparkConf conf = new SparkConf().setAppName("K-means Example").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);

		// Load and parse data
		String path = "WordMatrix/WordMatrix_brawner-s.txt";
		JavaRDD<String> data = sc.textFile(path);
		JavaRDD<Vector> parsedData = data.map(new Function<String, Vector>() {
			public Vector call(String s) {
				String[] sarray = s.split(",");
				double[] values = new double[sarray.length];
				for (int i = 0; i < sarray.length; i++)
					values[i] = Double.parseDouble(sarray[i]);
				return Vectors.dense(values);
			}
		});
		parsedData.cache();
		System.out.println(parsedData.collect());
		// Cluster the data into two classes using KMeans
		int numClusters = 10;
		int numIterations = 100;
		KMeansModel clusters = KMeans.train(parsedData.rdd(), numClusters, numIterations);

		// Evaluate clustering by computing Within Set Sum of Squared Errors
		double WSSSE = clusters.computeCost(parsedData.rdd());
		System.out.println("Within Set Sum of Squared Errors = " + WSSSE);
		// System.out.println(clusters.predict(parsedData).collect());

		File outFile;
		BufferedWriter bufferWriter;
		try {
			outFile = new File("TopicProbsKMeans.txt");
			if (!outFile.exists()) {
				outFile.createNewFile();
			}

			FileWriter fileWriter = new FileWriter(outFile.getName(), true);
			bufferWriter = new BufferedWriter(fileWriter);
			bufferWriter.write("" + clusters.predict(parsedData).collect());
			bufferWriter.close();
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 

		}
}