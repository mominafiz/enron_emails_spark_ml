﻿
"""
Created By: Afiz Momin / B00691752
References:
    http://applieddatamining.blogspot.ca/2013/06/nlp-using-python-and-nltk.html
    http://www.bogotobogo.com/python/NLTK/tokenization_tagging_NLTK.php

"""

import os
import re
import time
import sys
import random
import email
import datetime
from email.message import Message
from email.parser import Parser
import string
import unicodedata
try:
    import nltk
    from pyspark import SparkContext, SparkConf
except:
    pass
from nltk.corpus import stopwords


regex_email = re.compile(("([a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`"
                    "{|}~-]+)*(@|\sat\s)(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(\.|"
                    "\sdot\s))+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"))


def parseEmail(string):
    elist = (email[0] for email in re.findall(regex_email, string) if not email[0].startswith('//'))
    return [e for e in elist]

class DataPreProcess(object):
    """
        Data preprocessing class help with moving file in tree structure to 1st level of the root.
        It allows to get ListOfFiles from tree directory and parse email content for use.
    """

    def __init__(self, rootdir):
        self.rootpath = rootdir
        self.legitimizer = nltk.wordnet.WordNetLemmatizer()


    def moveFilesToUserRoot(self, foldername):
        """
            Move file from root directory to 1st level from the root.
        """
        for dir in os.listdir(self.rootpath):

            userdir = os.path.join(self.rootpath,dir)

            if not self.pathExists(os.path.join(userdir,foldername)):
                os.mkdir(os.path.join(userdir,foldername))

            for root, subfold, files in os.walk(userdir):
                for efile in files:
                    old = os.path.join(root,efile)
                    new = os.path.join(userdir,foldername)
                    if self.pathExists(old) and self.pathExists(new):
                        os.rename(old,os.path.join(new,efile + str(random.randint(0,999))))


    def getListOfFiles(self):
        """
            Generator function allows to retreive file from tree directory on demand.
        """
        for dir in os.listdir(self.rootpath):
            userdir = os.path.join(self.rootpath,dir)
            print "Current User directory: " + str(userdir)
            for root, subfold, files in os.walk(userdir):
                for file in files:
                    yield (userdir,os.path.join(root,file))


    def parseEmailForPayloadAndHeaders(self, fileObj):
        """
            Use this function to retreive email headers and body from the email text.
            It also supports multipart emails. It cleans trails of replies in each
            email, if exists.
        """

        msg = email.message_from_file(fileObj)
        
        # Order: From, To, Subject, CC, BCC, Total words in text, length of the
        # text
        # other statistical info: From, To, Min words in text, max words in
        # text, Avg.  words in text, Frequency of comm,
        headers = ['NA' for i in range(7)]

        for htuple in msg._headers:
            if htuple[0].lower() == 'from':   
                headers[0] = ','.join(parseEmail(htuple[1]))
            elif htuple[0].lower() == 'to':
                headers[1] = ','.join(parseEmail(htuple[1]))
            elif htuple[0].lower() == 'subject':
                headers[2] = htuple[1].strip().replace('\n', ' ').replace('\r',' ')
            elif htuple[0].lower() == 'cc':
                headers[3] = ','.join(parseEmail(htuple[1]))
            elif htuple[0].lower() == 'bcc':
                headers[4] = ','.join(parseEmail(htuple[1]))

        body = None
        if not msg.is_multipart():
            body = msg.get_payload()
            headers[6] = str(len(body))
            return headers, body
        else: 
            for part in msg.walk():
                if part.get_main_type() == 'multipart':
                    continue
                body += part.get_payload()
            
            headers[6] = str(len(body))
            return headers, body


    def removeStopWords(self,tokens=[]):
        """
            Removing words that from non-linguistic view do not carry any
            information. 
        """

        tokens = [t for t in tokens \
                    if not t in stopwords.words('english') and \
                    not (t.startswith('http') or t.startswith('www')) \
                    and not t.isdigit()]
        return tokens
    
        
    def tokenizeAndLowerCase(self, text):
        if text is None:
            return []
        
        # tokenize and remove punctuations
        tokens = nltk.word_tokenize(text.lower().translate(None,string.punctuation))
        
        # trim each token
        tokens = [t.strip() for t in tokens]
        return tokens


    def doStemming(self,tokens=[]):
        """
            Reduce words to the base form. Also called normalization.
            Use this function for stemming the text.
        """
        porter = nltk.PorterStemmer()
        tokens = [unicodedata.normalize('NFKD', porter.stem(t)).encode('ascii','ignore') for t in tokens]
        return tokens


    def legitimizeTokens(self, tokens=[]):
        tokens = [self.legitimizer.lemmatize(t) for t in tokens]       
        return tokens

    def runOnPython(self):    
        start = datetime.datetime.now()
        filegen = self.getListOfFiles()
    
        while True:
            # get user's root directory and filepath
            userdir, fp = filegen.next()
        
            # get the name of user's root directory path
            userdir_name = userdir.split(os.sep)[-1]
        
            # move one directory above user's root dir
            above_rootdir_path = '/'.join(userdir.split(os.sep)[:-2])
        
            # open file and send it for parsing
            with open(fp,'r') as infile, \
                open(os.path.join(above_rootdir_path,userdir_name + '_text.txt'),'a') as outtextfile, \
                open(os.path.join(above_rootdir_path,userdir_name + '_headers.txt'),'a') as outheaderfile:
            
                headers, text = self.parseEmailForPayloadAndHeaders(infile)      
        
                tokens = self.tokenizeAndLowerCase(text)          
                headers[5] = str(len(tokens))
         
                tokens = self.removeStopWords(tokens)
       
                line = ' '.join(tokens) + '\n'

                if line.strip().startswith("forward"):
                    continue

                outtextfile.write(line)
                outheaderfile.write('\t'.join([t for t in headers]) + '\n')

        total_time = (datetime.datetime.now() - start).total_seconds() / 60
        print "Total time in minutes: " + str(total_time)

    def runOnSpark(self):
        raise NotImplementedError("This features is currently unavailable")



def checkArgs(args):

    if args[-1].lower() == 'spark' or args[-1].lower() \
         and len(args) == 3 and pathExists(args[-2]):
        return args[-1].lower(), args[-2]
    else:
        print "Usage: <py/spark> DataPreProcess.py <filepath> <run on>"
        print "  e.g To run on python: python DataPreProcess.py <filepath> py"
        print "  e.g To run on spark: spark-submit DataPreProcess.py <filepath> spark"
 
def pathExists(path):
    if os.path.exists(path):
        return True
    else: 
        print "Root directory does not exists."
        sys.exit(1)
          

if __name__ == '__main__':
    
    rtuple = checkArgs(sys.argv)
    if rtuple is None :
        sys.exit(1)
    
    option, rootpath = rtuple
    if option is None or rootpath is None:
        sys.exit(1)
        
    
    dppObj = DataPreProcess(rootpath)
    
    if option == 'py':
        dppObj.runOnPython()
        
    elif option == 'spark':
        dppObj.runOnSpark()

    sys.exit(1)


