﻿import os
import random
import sys
from pyspark import SparkContext, SparkConf
#from pyspark.mllib.clustering import KMeans, KMeansModel
from DataPreProcess import pathExists
import unicodedata
import re
from operator import add


APP_NAME = "EnronAnalysis"

listOfStatsInfo = []
user_stats = {}

"""
def aggregateReceiversEmailUsingRegex(instance=[]):
    regex = re.compile(("([a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`"
                    "{|}~-]+)*(@|\sat\s)(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(\.|"
                    "\sdot\s))+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"))

    keypair = []
    if len(instance) == 7:
        elist = (email[0] for email in re.findall(regex, ' '.join(instance[1:])) if not email[0].startswith('//'))

        for e in elist:
            # remove instance if from or to email doesn't exists
            if instance[0] != '' or instance[0] is not None:
                keypair.append((instance[0], e))

    return keypair
"""

def getWordsInEmailsSentByEachContact(headerInfos):      
    
    def keypair(inst):
        if len(inst) == 7:
            return inst[0],long(inst[5])
        else:
            return 'error',1

     
    words_in_emails_for_each_user = headerInfos.map(lambda inst: keypair(inst))\
                    .groupByKey().collect()

    return words_in_emails_for_each_user
    
def averageWordsInEmailSentByEachContact(words_in_emails_for_each_user,usertupleList):
    valid_emails = [v[1] for v in usertupleList]
    user_list = []
   
    for utuple in words_in_emails_for_each_user:
        i,sum = float(0),0
        for vlist in utuple[1]:
            sum+= vlist
            i+= 1
        user_list.append(tuple((utuple[0],sum / i)))

    return [user for user in user_list if user[0] in valid_emails]

    
def getMinMaxWordsInEmailSentByEachContact(words_in_emails_for_each_user,usertupleList): 
    valid_emails = [v[1] for v in usertupleList]
    user_list = []
    
    for utuple in words_in_emails_for_each_user:
        temp = []
        for val in utuple[1]:
            temp.append(val)
        temp = sorted(temp)
        user_list.append(tuple((utuple[0], temp[0],temp[-1])))

    return [user for user in user_list if user[0] in valid_emails]


def __getPairCountsForEmailsSentByEachContact(headerInfos):
    def combineEmails(inst):
        combine = []
        if len(inst) == 7:          
            if inst[1].strip() != "NA":
                combine.extend(inst[1].split(','))
            if inst[3].strip() != "NA":
                combine.extend(inst[3].split(','))
            if inst[4].strip() != "NA":
                combine.extend(inst[4].split(','))
        return combine

    # Create key value pair of sender and receiver (includes To, CC, BCC)
    # calculates number of emails sent to each receipient by each sender
    pair_count = headerInfos.map(lambda inst: (inst[0],combineEmails(inst))) \
                .map(lambda line: [(line[0],val) for val in line[1]]) \
                .flatMap(lambda x: x).map(lambda x: (x,1)) \
                .reduceByKey(lambda x,y: x + y).collect()
    return pair_count
    

def getTotalUniqueContactsForEachContact(pair_counts,userlist):

    validemails = [email[1] for email in userlist]
    def filterByEmail(inst=['']):
        if inst[0] in validemails and 'enron.com' in inst[0]:
            return True
        else:
            return False

    pair_counts = sc.parallelize(pair_counts)
    # Simplify the output of pair-counts and get total unique contacts for each
    # enron employee
    contact_for_each_user = pair_counts.map(lambda x: (x[0][0],1)).reduceByKey(lambda x,y:x + y) \
                            .filter(filterByEmail).collect()


    return contact_for_each_user

def getAvgNumberOfEmailsSentToEachContact(pair_counts,userlist):
    
    validemails = [email[1] for email in userlist]
    def filterByEmail(inst=['']):
        if inst[0] in validemails and 'enron.com' in inst[0]:
            return True
        else:
            return False

    pair_counts = sc.parallelize(pair_counts)

    # times each contact contact has sent an email to any recepient
    each_contact_email_count = pair_counts.map(lambda x: (x[0][0],x[1])) \
                                        .reduceByKey(lambda x,y: x + y) \
                                        .filter(filterByEmail).collect()

    # times each contact communicated with recepient
    time_each_contact_count = pair_counts.map(lambda x:(x[0][0],1)).reduceByKey(lambda x,y: x + y).collect()

    clist = []
    for contact in each_contact_email_count:
        for other in time_each_contact_count:
            if contact[0] == other[0]:
                clist.append(tuple([contact[0], float(contact[1]) / other[1]]))

    return clist

def getMinMaxNumberOfEmailsSentToEachContact(pair_counts,userlist):

    validemails = [email[1] for email in userlist]
    def filterByEmail(inst=['']):
        if inst[0] in validemails and 'enron.com' in inst[0]:
            return True
        else:
            return False

    pair_counts = sc.parallelize(pair_counts)
    email_sent_by_each_contact = pair_counts.map(lambda x: (x[0][0],x[1])).groupByKey() \
                                .filter(filterByEmail).collect()

    email_sent_by_each_contact = [(e[0],sorted(e[1])) for e in email_sent_by_each_contact ]
    
    return [(e[0],e[1][0],e[1][-1]) for e in email_sent_by_each_contact]

def getTotalEmailsSentByEachContact(headerInfos,userlist):
    validemails = [email[1] for email in userlist]
    def filterByEmail(inst=['']):
        if inst[0] in validemails and 'enron.com' in inst[0]:
            return True
        else:
            return False

    # get total email sent from enron employee (From header)
    email_sent = headerInfos.map(lambda inst: (inst[0], 1)).filter(filterByEmail)\
                    .reduceByKey(lambda x,y: x + y).collect()
    return email_sent

def getTotalEmailsReceivedByEachContact(headerInfos,userlist):

    validemails = [email[1] for email in userlist]

    def combineEmails(inst):
        combine = []
        if len(inst) == 7:          
            if inst[1].strip() != "NA":
                combine.extend(inst[1].split(','))
            if inst[3].strip() != "NA":
                combine.extend(inst[3].split(','))
            if inst[4].strip() != "NA":
                combine.extend(inst[4].split(','))
        return combine

    
    def filterByEmail(inst=['']):
        if inst[0] in validemails and 'enron.com' in inst[0]:
            return True
        else:
            return False
    # check all email for enron employees in To, CC, and Bcc
    email_received = headerInfos.flatMap(lambda x: combineEmails(x)) \
                        .map(lambda x: (x,1)).reduceByKey(lambda x,y: x + y)\
                        .filter(filterByEmail).collect()
    return email_received


def processHeaderFile(listOfRdds=[], userlist=[]):
    """
        0. Users email address as the key ----
        For each user, I am interested in knowing 
        1. Avg Word Count in emails  -------
        2. Min Word Count in emails  ------
        3. Max Word Count in emails  -------
        4. Total unique contacts to whom email is sent
        5. Total emails sent
        6. Total emails received
        7. Avg number of emails sent to each contact
        8. Avg number of contacts in each email
        9. Min number of emails sent to each contact
        10. Max # of emails sent to each contact

    """

    # combine RDDs.
    headerRDD = reduce((lambda x,y: x.union(y)),listOfRdds)
    
    # remove unicode
    headerRDD = headerRDD.map(lambda x: unicodedata.normalize('NFKD',x).encode('ascii','ignore'))
    
    # split each instance
    headerRDD = headerRDD.map(lambda x: x.split('\t'))

    # persist data in memory for generating various stats
    headerInfos = headerRDD.persist()

    headers = [('Email Id','Avg Words In Emails Sent','Min Words In Emails Sent','Max Words In Emails Sent', \
                'Total Emails Sent','Total Emails Received', 'Total Unique Contacts', 'Avg. # Emails Sent',\
                'Min Email Sent To Each Contact', 'Max Email Sent To Each Contact')]
    emails = [e[1] for e in userlist]

    words_in_emails_for_each_user = getWordsInEmailsSentByEachContact(headerInfos)

    
    avg_word_list = averageWordsInEmailSentByEachContact(words_in_emails_for_each_user,userlist)
    avg_word_list = sorted(avg_word_list,key=lambda x: x[0])
    avg_word_list = [str(float(e[1])) for e in avg_word_list]

    min_max_word_list = getMinMaxWordsInEmailSentByEachContact(words_in_emails_for_each_user,userlist)
    min_max_word_list = sorted(min_max_word_list,key=lambda x: x[0])
    min_word_list = [str(float(e[1])) for e in min_max_word_list]
    max_word_list = [str(float(e[2])) for e in min_max_word_list]

    total_emails_sent = getTotalEmailsSentByEachContact(headerInfos,userlist)
    total_emails_sent = sorted(total_emails_sent,key=lambda x: x[0])
    total_emails_sent = [str(float(e[1])) for e in total_emails_sent]


    total_emails_received = getTotalEmailsReceivedByEachContact(headerInfos, userlist)
    total_emails_received = sorted(total_emails_received,key=lambda x: x[0])
    total_emails_received = [str(float(e[1])) for e in total_emails_received]

    pair_counts = __getPairCountsForEmailsSentByEachContact(headerInfos)
    
    total_unique_contacts = getTotalUniqueContactsForEachContact(pair_counts,userlist)
    total_unique_contacts = sorted(total_unique_contacts,key=lambda x: x[0])
    total_unique_contacts = [str(float(e[1])) for e in total_unique_contacts]

    avg_emails_sent = getAvgNumberOfEmailsSentToEachContact(pair_counts,userlist)
    avg_emails_sent = sorted(avg_emails_sent,key=lambda x: x[0])
    avg_emails_sent = [str(float(e[1])) for e in avg_emails_sent]

    min_max_email_sent = getMinMaxNumberOfEmailsSentToEachContact(pair_counts,userlist)
    min_max_email_sent = sorted(min_max_email_sent,key=lambda x: x[0])
    min_email_sent = [str(float(e[1])) for e in min_max_email_sent]
    max_email_sent = [str(float(e[2])) for e in min_max_email_sent]

    lines = headers
    lines.extend([row for row in zip(emails,avg_word_list, min_word_list,max_word_list,\
                        total_emails_sent,total_emails_received,total_unique_contacts,\
                        avg_emails_sent, min_email_sent,max_email_sent)])

    return lines


def checkArgs(args):

    if args[-1].isdigit() and len(args) == 3 and pathExists(args[-2]):
        return args[-1], args[-2]
    else:
        print "Usage: spark-submit DataDiscovery.py <directory path> <# of users> "
        print "  e.g spark-submit DataPreProcess.py /path/to/dir 10"
        print " To select all users, please enter 0 for # of user"
        

def pathExists(path):
    if os.path.exists(path):
        return True
    else: 
        print "Root directory does not exists."
        sys.exit(1)
          

def getUsers(n=0):
    users = []
    with open("UserAndEmail.txt","r") as infile:
        while True:
            try:
              line = infile.next()
              lsplit = line.split(",")
              users.append(tuple([lsplit[0].strip(),lsplit[1].strip()]))
            except StopIteration:
                break
    if n == 0:
        return users

    return random.sample(users,n)
    

def main(sc): 
    rtuple = checkArgs(sys.argv)
    if rtuple is None :
        sys.exit(1)
    
    n, data_path = rtuple
   
    if not pathExists(data_path):
        print "Unable to find directory."
        sys.exit(1)
    
    
    user_dirs = getUsers(int(n))
    listOfRdd_headers = []
    
    for user_dir in user_dirs:
        print user_dir
        listOfRdd_headers.append(sc.textFile(os.path.join(data_path,user_dir[0] + "_headers.txt")))
        

    lines = processHeaderFile(listOfRdd_headers, user_dirs)

    with open('UserStats.txt','w') as out:
        for l in lines:
            out.write('\t'.join(l) + '\n')

    sc.stop()

if __name__ == "__main__":
    conf = SparkConf().setAppName(APP_NAME)
    
    #in cluster this will be like
    #"spark://ec2-0-17-03-078.compute-#1.amazonaws.com:7077"
    conf = conf.setMaster("local[*]")
    
    #conf = {'fs.s3n.awsAccessKeyId': '', 'fs.s3n.awsSecretAccessKey':''}

    sc = SparkContext(conf=conf)
    main(sc)