﻿import sys
import os
from heapq import nsmallest

def getTopics(fp):
    
    topic_probs = readFile(fp)
    headers = readFile(os.path.join(fp.split(os.sep)[1],"..", "MatrixHeaders.txt"))

    cluster_topics = []
    for line in topic_probs:
        probs = line.strip().split(" ")
        
        ref = probs[:1]
        probs = probs[1:]

        probs_and_headers = zip(headers,probs)
               
        #probs = sorted(probs)
        #min(probs, key=lambda x:abs(x-float(ref)))
        
        # take 10 n closest to ref.  prob.
        topics = nnsmallest(10, probs_and_headers, key=lambda x: abs(float(x[1]) - float(ref)))
        topics = [tup[0] for tup in topics]

        cluster_topics.append(topics)

    return cluster_topics


def checkArgs(args):

    if len(args) == 2 and pathExists(args[-1]):
        return args[-1]
    else:
        print "Usage: <py/spark> TopicModeling.py <rootdir of matrix>"
        print "  e.g To run on python: python TopicModeling.py /path/to/root"
        
        sys.exit(1)
 
def pathExists(path):
    if os.path.exists(path):
        return True
    else: 
        print "Root directory does not exists."
        sys.exit(1)
          
def readFile(fp):
    with open(fp,"r") as r:
        return r.readlines()

def getUsers():
    users = []
    with open("UserAndEmail.txt","r") as infile:
        while True:
            try:
              line = infile.next()
              lsplit = line.split(",")
              users.append(tuple([lsplit[0].strip(),lsplit[1].strip()]))
            except StopIteration:
                break

    return users

if __name__ == "__main__":
    fp = checkArgs(sys.argv)[0]

    users = {}
    
    user_tuples = getUsers()
    
    if pathExists(fp):
        for root, subfold, files in os.walk(fp):
            for file in files:
                user_name = file.split(os.sep)
                print user_name
                print file
               
                email_id = [tup[1] for tup in user_tuples if user_name == tup[0]][0]
                
                users[email_id] = getTopics(file)
     
    with open("TopicsForEachUser.txt","w") as out:
        for k,v in users.iteritems():
            out.write(k + "\n")
            for l in v:
                out.write(','.join(l) + "\n")
            out.write("\n\n")