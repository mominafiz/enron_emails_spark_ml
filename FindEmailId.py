﻿
from pyspark import SparkContext, SparkConf

import re,os,unicodedata

def __getPairCountsForEmailsSentByEachContact(headerInfos, searchstr, init):
    
    def combineEmails(inst):
        combine = []
        if len(inst) == 7:  
                    
            if inst[1].strip() != "NA":
                combine.extend(inst[1].split(','))
            if inst[3].strip() != "NA":
                combine.extend(inst[3].split(','))
            if inst[4].strip() != "NA":
                combine.extend(inst[4].split(','))
            combine.extend(inst[0].split(','))
        return list(set(combine))

    # Create key value pair of sender and receiver (includes To, CC, BCC)
    # calculates number of emails sent to each receipient by each sender
    pair_count = headerInfos.map(lambda inst: (inst[0],inst[0])) \
                .filter(lambda x: searchstr in x[0] and 'enron.com' in x[0] )\
                .flatMap(lambda x:x).distinct().collect()
                #.flatMap(lambda x: x[1]) \
    # pair_count = headerInfos.filter(lambda x: searchstr in x and 'enron.com' in x ).distinct().collect()
                #.map(lambda x: (x,1)).reduceByKey(lambda x,y: x + y).collect()

    with open("UserAndEmail.txt","a") as outfile:
        for val in pair_count:
            i = val.index("@")
            name_idx=None
            valsplit = val[:i].split(".")
            try:
                name_idx = valsplit.index(searchstr)
            except:
                continue
            if len(valsplit)==2:
                if valsplit[1-name_idx].startswith(init):
                    outfile.write(searchstr+"-"+init+","+val+"\r\n")
            
            

def getTenUsers():
    users = []
    with open("user_lists.txt","r") as infile:
        while True:
            try:
              line = infile.next()
              users.append(line.strip())
            except StopIteration:
                break
    return users

def main(sc): 
    data_path = "/home/ubuntu/AssignmentDocs/email-dataset"

    user_dirs = getTenUsers()
    
    for user_dir in user_dirs:
        print user_dir
        rdd = sc.textFile(os.path.join(data_path,user_dir + "_headers.txt"))
        # remove unicode
        rdd = rdd.map(lambda x: unicodedata.normalize('NFKD',x).encode('ascii','ignore'))
        # split each instance
        rdd = rdd.map(lambda x: x.split('\t'))
     
        __getPairCountsForEmailsSentByEachContact(rdd,user_dir.split('-')[0], user_dir.split('-')[1])


if __name__ == "__main__":
    APP_NAME = "FindEmailId"
    conf = SparkConf().setAppName(APP_NAME)
    
    #in cluster this will be like
    #"spark://ec2-0-17-03-078.compute-#1.amazonaws.com:7077"
    conf = conf.setMaster("local[*]")
    
    #conf = {'fs.s3n.awsAccessKeyId': '', 'fs.s3n.awsSecretAccessKey':''}

    sc = SparkContext(conf=conf)
    main(sc)